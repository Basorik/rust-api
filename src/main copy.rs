use axum::{extract::Query, http::Method, response::Html, routing::get, Router};
use serde::Deserialize;
use std::net::SocketAddr;

mod http;

#[tokio::main]
async fn main() {
    // build our application with a single route
    let app = Router::new()
        .route("/", get(root))
        .route("/api", get(api));



    #[derive(Deserialize)]
    struct Payload {}
    // run our app with hyper, listening globally on port 3000
    async fn root(
        range: Option<Query<http::RangeParameters>>,
        method: Method,
        body: Option<String>
    ) -> Html<String> {
        http::print_rand(range, &method, &body)
    }

    async fn api() -> Html<String> {
        let resp = reqwest::get("https://api.spotify.com/v1/search").await;
        match resp {
            Ok(resp) => Html(format!("Response: {:?}", resp)),
            Err(_) => Html(format!("Error"))
            
        }
    }

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    println!("listening on {}", addr);

    let listener = tokio::net::TcpListener::bind(&addr)
        .await
        .unwrap();
    axum::serve(listener, app)
        .await.
        unwrap();
}
