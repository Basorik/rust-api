use axum::{extract::Query, http::Method, response::Html};
use rand::{thread_rng, Rng};
use serde::Deserialize;


#[derive(Deserialize)]
pub struct RangeParameters {
    pub start: usize,
    pub end: usize,
}

impl Default for RangeParameters {
    fn default() -> Self {
        Self { start: (1), end: (100) }
    }
}


pub fn print_rand(range: Option<Query<RangeParameters>>, method: &Method, body: &Option<String>) -> Html<String>{
    let Query(range) = range.unwrap_or_default();
    let random_number = thread_rng().gen_range(range.start..range.end);
    let body = match body {
        Some(body) => body,
        None => "No Body"
    };
    Html(format!("<h1>Random Number: {} - Method: {} - - Body: {} </h1>", random_number, method, body))

}