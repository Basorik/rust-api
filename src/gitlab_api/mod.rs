use std::fmt::{Display, Error as FmtError, Formatter};
use std::error::Error;
use reqwest::{Client, Error as ReqError, Response};
use url::{ParseError, Url};
#[derive(Debug)]
pub struct GitlabClient {
    pub url_root: Url,
    client: Client,

}

#[derive(Debug)]
pub enum ErrorCodes {
    RequestError(ReqError),
    UrlParseError(ParseError)
}

impl From<ParseError> for ErrorCodes {
    fn from(e: ParseError) -> Self {
        Self::UrlParseError(e)
    }
}

impl From<ReqError> for ErrorCodes {
    fn from(e: ReqError) -> Self {
        Self::RequestError(e)
    }
}

impl Error for ErrorCodes {}

impl Display for ErrorCodes {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        match  self {
            Self::RequestError(e) => write!(f, "Error in request: {}", e),
            Self::UrlParseError(e) => write!(f, "Error parsing url: {}", e)
            
        }
    }
}

impl GitlabClient {
    pub fn new(url_root: &str) -> Result<Self, ParseError> {
        
        let url_root = Url::parse(url_root);
        match url_root {
            Ok(url_root) => Ok(Self { 
                                url_root,
                                client: Client::new()
                            }),
            Err(e) => return Err(e)
            
        }
    }

    pub async fn get(&self, endpoint: &str) -> Result<Response, ErrorCodes>{
        let full_url = match self.url_root.join(endpoint) {
            Ok(r) => r,
            Err(e) => return Err(ErrorCodes::UrlParseError(e))
        };
        match self.client.get(full_url).send().await {
            Ok(r) => return Ok(r),
            Err(e) => return Err(ErrorCodes::RequestError(e))
        };
    }
}